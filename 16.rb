require_relative 'helper'
require 'crypto'

@aes = OpenSSL::Cipher.new("AES-128-CBC")
@key = randbytes(16)

def func1(input)
	raise if input.match(/[;=]/)
	str = "comment1=cooking%20MCs;userdata=#{input};comment2=%20like%20a%20pound%20of%20bacon".pad(:PKCS7, 16)
	@aes.encrypt
	@aes.key = @key
	@aes.update(str) + @aes.final
end


def func2(ctext)
	@aes.decrypt
	@aes.key = @key
	str = @aes.update(ctext) + @aes.final

	str.split(';').each do |e|
		return true if e.split('=').first == "admin"
	end
	return false
end


blocksize = 16

input = "X"
str = "comment1=cooking%20MCs;userdata=#{input};comment2=%20like%20a%20pound%20of%20bacon"

want = ";admin=true;"

x = str.slice(blocksize, want.length).xor(want)

ctext = func1(input)
cctext = ctext[0...want.length].xor(x) + ctext[want.length..-1]

check(func2(cctext), true)

