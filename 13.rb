require_relative 'helper'
require 'openssl'
require 'crypto'

def profile_for(email)
	h = {
		email: email,
		uid: rand(10),
		role: 'user'
	}

	raise "no meta-char" if email.match(/[&=]/)

	return h.map {|k,v| "#{k}=#{v}"}.join("&")
end

@key = randbytes(16)
@aes = OpenSSL::Cipher.new("AES-128-ECB")

def encrypt(email)
	@aes.encrypt
	@aes.key = @key
	@aes.update(profile_for(email))+@aes.final
end

def decrypt(ctext)
	@aes.decrypt
	@aes.key = @key
	@aes.padding = 0
	enc = @aes.update(ctext)+@aes.final

	h = {}
	m = enc.split("&").each do |e|
		if e.match(/(\w+)=(\w+)/)
			h[$1.to_sym] = $2
		end
	end
	h
end

blocksize = 16
outblocksize = 16

piece = "0"*(blocksize-"email=".length)
piece += "admin".pad(:PKCS7, blocksize)
admin = encrypt(piece)

fixlen = "email=foo@bar.com&uid=1&role=".length
len = blocksize - (fixlen % blocksize)

myid = encrypt("foo#{"0"*len}@bar.com")

payload = myid[0...-outblocksize] + admin[outblocksize...outblocksize*2]

chal = decrypt(payload)

check(chal[:role], "admin")
