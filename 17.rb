require_relative 'helper'
require 'crypto'

# CBC Padding Oracle

@str = %w(MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=
MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=
MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==
MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==
MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl
MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==
MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==
MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=
MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=
MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93)

@aes = OpenSSL::Cipher.new("AES-128-CBC")
@blocksize = 16
@key = randbytes(@blocksize)
@iv = randbytes(@blocksize)

def func1(n=rand(10))
	s = @str[n].pad(:PKCS7, 16)
	iv = randbytes(16)

	@aes.encrypt
	@aes.key = @key
	@aes.iv = iv
	@aes.padding = 0

	return @aes.update(s) + @aes.final
end

def func2(ctext)
	@aes.decrypt
	@aes.key = @key
	ptext = @aes.update(ctext) + @aes.final

	validate_pkcs(ptext)
end

def padding_oracle(ctext)
	lastbyte = ctext[-@blocksize-1]
	r = []
	256.times do |i|
		next if i == lastbyte.ord
		ctext[-@blocksize-1] = i.chr
		r.push(i) if func2(ctext)
	end
	r
end

@str.each_with_index do |s, i|
	ctext = func1(i)
	oracle = padding_oracle(ctext.dup)
	binding.pry if oracle.length > 1

	pad = oracle.first ^ ctext[-@blocksize-1].ord ^ 1

	check(pad, s.pad(:PKCS7, 16)[-1].ord)
end

