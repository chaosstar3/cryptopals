require_relative 'helper'
require 'base64'
require 'crypto'

prob = File.read("6.txt")
prob = Base64.decode64(prob)

keysize, res = guess_xor_keysize(prob)

ans, key, d = dexor(prob)

check(key, "Terminator X: Bring the noise")
