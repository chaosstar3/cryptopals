require_relative 'helper'

result = []
File.open("8.txt").each do |line|
	cnt = Hash.new(0)
	line.chomp.scan(/.{32}/).each {|s| cnt[s] += 1}
	v = cnt.values
	if not v.empty? and v.max > 1
		result << line
	end
end

check(result[0][0..7], "d8806197")
