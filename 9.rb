require_relative 'helper'
require 'crypto'

prob = "YELLOW SUBMARINE"
answer = "YELLOW SUBMARINE\x04\x04\x04\x04"

chal = prob.pad(:PKCS7, 20)

check(chal, answer)
