require_relative 'helper'
require 'base64'

# An ECB/CBC detection oracle

def aes128_encrypt(ptext, key, isecb)
	aes = nil
	if isecb
		aes = OpenSSL::Cipher.new("AES-128-ECB")
	else
		aes = OpenSSL::Cipher.new("AES-128-CBC")
		aes.iv = randbytes(16)
	end
	aes.encrypt
	aes.key = key
	aes.update(ptext)+aes.final
end

def rand_pad(text)
	# pre-pad
	padlen = rand(6) + 5
	pad = []
	padlen.times { pad << rand(256) }
	ptext = pad.pack("C*") + text

	# post pad
	padlen = rand(6) + 5
	pad = []
	padlen.times { pad << rand(256) }
	ptext + pad.pack("C*")
end

text = "a" * 32
key = randbytes(16)
ecb = aes128_encrypt(text, key, true)

64.times do
	isecb = (rand(2) == 0)
	ctext = aes128_encrypt(rand_pad(text), key, isecb)
	check(isecb, aes_mode_oracle(ecb, ctext))
end



