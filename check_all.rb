exclude = [__FILE__] + %w(helper.rb)

cnt = 0
suc = 0
ign = 0

(Dir["*.rb"]-exclude).each do |f|
	ret = `ruby #{f}`
	status = $?.exitstatus
	case status
	when 0xff
		puts "#{f}\tIgnore"
		ign += 1
	when 0
		puts "#{f}\tPass: #{ret}"
		suc += 1
	else
		puts "#{f}\tFail: #{ret}"
	end
	cnt += 1
end

puts "#{cnt} Tests: #{suc} passed / #{cnt-suc-ign} failed / #{ign} ignored"
