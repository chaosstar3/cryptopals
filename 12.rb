require_relative 'helper'
require 'base64'
require 'crypto'

@ptext = Base64.decode64(
"Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg
aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq
dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK")
@key = randbytes(16)

def encryption(input)
	aes = OpenSSL::Cipher.new("AES-128-ECB")
	aes.encrypt
	aes.key = @key
	aes.update(input + @ptext) + aes.final
end

# detect mode
e1 = encryption("A"*32)
e2 = encryption("A"*64)

isecb = aes_mode_oracle(e1, e2)
check(isecb, true)

# detect block length
ctext = encryption("")
outsize = ctext.length
padsize = 1
blocksize = 1
outblocksize = 0
loop do
	e = encryption("A"*padsize)
	if e.length > outsize
		outblocksize = e.length - outsize
		outsize = e.length
		break
	end
	padsize += 1
end
loop do
	e = encryption("A"*(padsize+blocksize))
	break if e.length > outsize
	blocksize += 1
end

outsize = ctext.length
ptextlen = blocksize * (outsize / outblocksize) - padsize

check(ptextlen, @ptext.length)

# decrypt

decrypted = []
ptextlen.times do |i|
	try = encryption("A"*(padsize+i+1))

	testsize = try.length - outsize

	256.times do |b|
		teststr = ([b]+decrypted).pack("C*").pad(:PKCS7, blocksize)
		find = encryption(teststr)

		if try[outsize..-1] == find[0...testsize]
			decrypted.unshift(b)
			break
		end
	end
end

check(decrypted.pack("C*"), @ptext)

