require_relative 'helper'
require 'base64'
require 'openssl'

prob = File.read("7.txt")
prob = Base64.decode64(prob)
key = "YELLOW SUBMARINE"

d = aes128ecb_decrypt(prob, key)

check(d.split("\n").first, "I'm back and I'm ringin' the bell ")
