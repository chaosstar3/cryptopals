require_relative 'helper'

# PKCS#7 padding validation

def validate(str)
	blocksize = 16

	a = str.unpack("C*")

	return false if a.length % blocksize != 0

	last = a[-1]

	return false if last > blocksize

	(last-1).times do |i|
		return false if last != a[-(2+i)]
	end

	return true
end

check(validate("ICE ICE BABY\x04\x04\x04\x04"), true)
check(validate("ICE ICE BABY\x05\x05\x05\x05"), false)
check(validate("ICE ICE BABY\x01\x02\x03\x04"), false)


