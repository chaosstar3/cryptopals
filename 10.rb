require_relative 'helper'
require 'base64'

ct = File.read("10.txt")
ct = Base64.decode64(ct)
key = "YELLOW SUBMARINE"
iv = "\0"*16

pt = aes128cbc_decrypt(ct, key, iv)

check(pt[0...16], "I'm back and I'm")
