require_relative 'helper'
require 'openssl'
require 'crypto'

# AES-128-ECB(random-prefix || attacker-controlled || target-bytes, random-key)

@target = "Target Key message"
@prefix = randbytes(rand(16)+1)
@key = randbytes(16)

def encrypt(input)
	aes = OpenSSL::Cipher.new("AES-128-ECB")
	aes.encrypt
	aes.key = @key
	aes.update(@prefix + input + @target) + aes.final
end

blocksize = 16

e0 = encrypt("")
e1 = encrypt("a")

first_common_block = 0
loop do
	if e0[first_common_block] == e1[first_common_block]
		first_common_block += 1
	else
		break
	end
end

first_common_block = first_common_block/blocksize*blocksize
cmp_range = first_common_block...first_common_block+blocksize

prefix_pad_size = 2
eprev = e1
loop do
	e = encrypt("a"*prefix_pad_size)

	if eprev[cmp_range] == e[cmp_range]
		prefix_pad_size -= 1
		break
	else
		eprev = e
		prefix_pad_size += 1
	end
end

first_common_block += blocksize

e0 = encrypt("a"*prefix_pad_size)
# detect len
pad_size = 1
loop do
	e = encrypt("a"*(prefix_pad_size + pad_size))
	break if e.length > e0.length
	pad_size += 1
end

targetlen = blocksize * ((e0.length - first_common_block) / blocksize) - pad_size

check(targetlen, @target.length)

# decrypt
decrypted = []
targetlen.times do |i|
	trysize = prefix_pad_size + pad_size + i + 1
	try = encrypt("A"*trysize)

	testsize = try.length - e0.length

	256.times do |b|
		testbytes = ([b]+decrypted).pack("C*").pad(:PKCS7, blocksize)
		find = encrypt("A"*prefix_pad_size+testbytes)
		if try[e0.length..-1] == find[first_common_block...first_common_block+testsize]
			decrypted.unshift(b)
		end
	end
end

check(decrypted.pack("C*"), @target)

