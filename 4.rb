require_relative 'helper'
require 'crypto'

result = []
max = {v: 0}
File.open("4.txt","r").each do |line|
	ans, key, hd = dexor(line.chomp.unhex, [1])

	score = key.length.times.inject(0) do |s, i|
		s + hd[:key][i][key[i].ord]
	end

	if score > max[:v]
		max[:i] = [ans, key]
		max[:v] = score
	end
end

ans, key = max[:i]

check(ans, "Now that the party is jumping\n")
check(key, "5")
