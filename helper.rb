require 'macro' # rubylib
require 'openssl'
require 'algorithm'
require 'pry'

@ck_succ = 0
@ck_fail = 0
at_exit do
	total = @ck_succ + @ck_fail

	# no test did
	exit 0xff if total == 0

	puts "#{@ck_succ}/#{total} test passed"
	exit @ck_fail
end

def check(chal, answer)
	if chal != answer
		puts "Wrong"
		puts "#{caller[-1]}"
		print "given: "; p chal
		print "expect: "; p answer
		@ck_fail += 1
	else
		@ck_succ += 1
	end
end

def randbytes(len)
	r = []
	len.times do
		r << rand(256)
	end
	r.pack("C*")
end

# challenge 7
def aes128ecb_decrypt(ctext, key)
	aes = OpenSSL::Cipher.new("AES-128-ECB")
	aes.decrypt
	aes.key = key
	aes.padding = 0
	aes.update(ctext)+aes.final
end

# challenge 10
def aes128cbc_decrypt(ctext, key, iv)
	blocksize = 32
	ptext = ""
	0.step(ctext.length-1, blocksize) do |i|
		block = ctext[i...i+blocksize]
		d = aes128ecb_decrypt(block, key).xor(iv)
		iv = block
		ptext << d
	end
	return ptext
end

# challenge 11
def aes_mode_oracle(ecb, ctext)
	return longest_common_substring(ecb, ctext).first >= 16
end


